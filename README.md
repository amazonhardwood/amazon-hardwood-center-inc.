Amazon Hardwood Center has gained much experience and reputation ever since the journey began from 1999. We have served the GTA with high quality products from the top flooring brands of Canada. Ask for any variety of flooring and we have them at competitive prices, making them widely accessible to the masses. Moreover, any query or requirements you have, our customer care representatives can ensure you are rendered satisfied with the solutions. You can visit any of our five locations in GTA to experience our unparalleled services and products.

Website : https://www.amazonhardwood.ca/
